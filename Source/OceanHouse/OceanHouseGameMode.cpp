// Copyright Epic Games, Inc. All Rights Reserved.

#include "OceanHouseGameMode.h"
#include "OceanHouseHUD.h"
#include "OceanHouseCharacter.h"
#include "UObject/ConstructorHelpers.h"

AOceanHouseGameMode::AOceanHouseGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Blueprint/Character/BP_Character"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AOceanHouseHUD::StaticClass();
}
