// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "OceanHouseGameMode.generated.h"

UCLASS(minimalapi)
class AOceanHouseGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AOceanHouseGameMode();
};



